#!/bin/bash

jour=$1

cd ~/berlin/static/tracks
./merge.sh

cd ~/berlin/static/media/photos/
pic_folder=day$jour
mkdir $pic_folder

cd $pic_folder
nautilus . 2> /dev/null &
read -e -p "cover:" cover
cover=$(echo $cover | sed "s/JPG/jpg/g")

cd ~/berlin/static/media/photos/day$jour
mogrify -quality 50 -format jpg * && rm *.JPG

cd ~/berlin/static/media/
./convert.sh $jour

cd ~/berlin/content/post/

date_jour_txt=`date +"%Y-%m-%d"`
date_jour_full=$date_jour_txt"T10:17:43Z"

echo "+++
title = \"Jour $jour\"
date = $date_jour_full
draft = false
author =
cover = \"day$jour.jpg\"
place = \"\"
summary = \"\"
map = true
+++

## Etape 

## Kilomètres

## Météo

## Phrase du jour

## Résumé
" > day$jour.md

cd ~/berlin/data
echo "[[entries]]
	name = \"Jour $jour\"
	path = \"day$jour\"
	thumb = \"$cover\"
" | cat - gallery.toml > temp && mv temp gallery.toml

cp ~/berlin/static/media/photos/day$jour/$cover ~/berlin/static/covers/day$jour.jpg
