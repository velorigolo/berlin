+++
title = "Jour 4"
date = 2021-09-09T10:17:43Z
draft = false
author = "Quentin"
cover = "day4.jpg"
place = "Koszalin"
summary = "La quête du sklep"
map = true
+++

## Etape 

Pobierowo - Koszalin

## Kilomètres

102

## Météo

Fait chaud mais on est à l'ombre 

## Phrase du jour

Hmm ils sont très bons ces harengs j'aimerais savoir les cuisiner comme ça -
Mathieu très fan des harengs polonais.

## Résumé

Après une bonne nuit et un autre excellent petit déjeuner à la limite de
l'indigestion, nous partons à 10h pile (un record) pour l'étape du jour qui
s'annonce très sympathique, longeant la mère par des pistes cyclables entre
les stations balnéaires.

Le départ est assez compliqué car nous jongleons entre cyclables récentes,
avenues pleines de plagistes (et donc enfants aux trajectoires aléatoires et
personnes âgées qui se traînent la bite en prenant toute la place), et
vieilles routes militaires pavées dans lesquelles nous nous perdons. Mais
globalement l'étape est vraiment belle, la côte baltique et la lumière rasante
rendant le spectacle très chouette.

Ça me fait un peu chier que les infrastructures pour vélo soit mieux foutues
en Pologne qu'en France mais je ravale mon chauvinisme en me disant que nous
au moins on a ramené la coupe à la maison.

Autre épreuve du jour, les nuées de moucherons qui me rappellent le Canada,
j'en piège des dizaines sur mes bras et jambes pendant que Mathieu qui essaie
de m'expliquer les dernières avancées dans la recherche sur la fusion atomique
en mange tout autant. Pour le consoler, à midi je lui prends du hareng
polonais en pique nique, il est extrêmement fan et s'attelle à découvrir la
recette en essayant des traductions à base de russe et d'allemand.

Ce qui nous amène au sujet principal qui nous a occupé aujourd'hui : que veut
dire sklep ? Ce mot qu'on retrouve marqué presque partout sur diverses
boutiques et hôtels nous a laissé perplexe. Soldes ? Bienvenue ? Ouvert ?
Disponible ? Ce débat nous tient animé pendant toute la fin de l'étape. À
demain pour de nouvelles aventures !
