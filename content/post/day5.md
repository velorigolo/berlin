+++
title = "Jour 5"
date = 2021-09-10T10:17:43Z
draft = false
author = "Mathieu"
cover = "day5.jpg"
place = "Bytow"
summary = "La roue de la fortune"
map = true
+++

## Etape 

Koszalin - Bytow

## Kilomètres

100

## Météo

Il fait bon à l'ombre

## Phrase du jour

Quelle étape de bytow

## Résumé

Depuis le début de ce voyage, nous jouons quotidiennement à la roue de la
fortune. Nous ne tentons pas de gagner un yacht, une croisière de rêve ou tout
autre produit dépravé de l'ultra-capitalisme. Nous n'achetons d'ailleurs pas
nos billets dans un bar PMU à l'aube avant de prendre la route, au milieu de
braves ouvriers Polonais. Non, nous ouvrons simplement Google Maps chaque soir.

C'est d'ordinaire Quentin qui se dévoue à cette tache. Le but est d'obtenir le
tracé GPS de la prochaine étape, que je m'évertuerai de suivre grâce à mon
téléphone. Si Google Maps excelle d'ordinaire dans la planification de trajets
routiers, il existe un mode beaucoup plus hasardeux ou l'aléatoire semble plus
de mise : le terrible mode "vélo".

Je crois avoir identifié les différents lots à gagner en utilisant ce mode, et
je vais vous faire part des mes découvertes.

Le premier lot, le plus commun est la belle route départementale ombragée, à
l'asphalte lisse et immaculé. Nous pouvons obtenir de belles moyennes sur ces
routes et nous ne nous soucions uniquement de quelques conducteurs un peu trop
véloces.

Le second lot est l'ancienne route composée de dalles de béton de 2 mètres de
large par 1 mètre de long et datant de l'époque ou la Pologne était un état
satellite de l'URSS. À la jonction de chaque dalle, soit tous les mètres, le
vélo se cabre quelque peu. Une petite technique, ou life hack pour les jeunes
lecteurs bourrins, est d'augmenter légèrement sa vitesse pour obtenir une
secousse continue. Avec un peu d'entraînement on finirait presque par s'y
habituer.

Le troisième lot est l'ancienne route pavée recouverte de sable pour plus de
plaisir. Bienvenue sur un chemin ou le vélo chasse de l'avant et de l'arrière
pendant que les pavés manquants forcent des trajectoires tout sauf
rectilignes.

Vient ensuite le lot sur lequel nous sommes tombés toute cette après midi, la
piste sablonneuse en foret. Cela ressemble un peu au lot précédent sauf que le
sable peut venir arrêter complètement le vélo et catapulter son
cavalier. Quentin a d'ailleurs pu pratiquer sa technique de réception à
l'épreuve de la catapulte. Il décroche un solide 8 sur 10 selon le jury.

Pour finir le lot le plus terrible, qui nous fait frémir d'inquiétude lorsque
nous chargeons la trace GPS dans le téléphone, la piste pavée qui doit dater
de l'époque romaine. Nous avons alors plus l'impression d'être dans la
centrifugeuse de la NASA pour tester notre résistance sous 10G, que dans une
foret Polonaise. Tout tremble, les sacoches se détachent, le téléphone saute
de son habitacle, les pneus crissent sous les vibrations, un vrai délice.

Bref, nous avons quand même beaucoup aimé cette étape, et nous prévoyons
d'arriver demain à Gdansk pour y passer un jour de repos bien mérité.

