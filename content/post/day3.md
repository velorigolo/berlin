+++
title = "Jour 3"
date = 2021-09-08T10:17:43Z
draft = false
author = "Quentin"
cover = "day3.jpg"
place = "Pobierowo"
summary = "Chocolat"
map = true
+++

## Etape 

Szczecin - Pobierowo

## Kilomètres

103

## Météo

Très bonne, soleil et pas trop chaud

## Phrase du jour

Ah désolé je passe dans un tunnel je te capte plus allo ? - Quentin qui refuse
d'écouter Mathieu

## Résumé

Nous nous levons ce matin dans notre hôtel de business très classe, direction
le petit déjeuner où nous nous retrouvons devant environ 7000 choix de
délicieux plats. Je tiens à tout essayer et Mathieu qui est plus sage me juge
quand je reviens avec ma 6ie assiette de fromage et œufs brouillés. Le départ
est difficile mais je me rassure en me disant que j'ai absorbé assez de
calories pour tenir durant 5 jours d'efforts soutenus.

L'étape est roulante et on avons vite retrouvé notre rythme. Nous longeons des
forêts de conifères sur une route très bien entretenue, mais qui longe une
nationale. Au moins c'est tout droit.

Mathieu me raconte qu'il a rencontré un homme incroyable qui fait des étapes
de 200 kilomètres en plus d'être plus beau plus intelligent et plus fort que
lui. Il semble le vivre assez mal et me propose de tenter une étape de 230
kilomètres demain. Je profite d'un des tunnels que nous traversons pour lui
faire le coup de la coupure de réseau. Mais Mathieu n'est pas dupe car il est
à 2 mètres de moi et attends la sortie du tunnel pour recommencer son
discours.

À midi rien en vue pour manger et nous nous arrêtons dans une station
service. J'achète une tablette de chocolat Milka dont j'aurais la chance de
goûter 1 demi carré avant que Mathieu l'engloutisse sous mes yeux. La suite de
l'étape est parfaite, si on excepte un chemin dégueulasse "et si on mettait
des dalles trouées en guise de route ce sera mieux que la terre non ?" (non)
qui détruit mon dos et nous arrivons dans une petite ville balnéaire avec des
touristes français avec tatouages tribaux et des vieux avec des bâtons de
marche. On se croirait à la Baule.
