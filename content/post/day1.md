+++
title = "Jour 1"
date = 2021-09-06T10:17:43Z
draft = false
author = "Quentin"
cover = "day1.jpg"
place = "Bernau"
summary = "Un départ classique"
map = true
+++

## Etape 

Paris - Berlin - Bernau
‎ 
## Kilomètres

24

## Météo

On sait pas il fait nuit

## Phrase du jour

Ça y'est Quentin nous sommes des digital clochards - Mathieu qui télétravaille
assis devant les toilettes du train

## Résumé

Et c'est parti pour un nouveau voyage à vélo ! Au programme cette fois-ci
Berlin - Talinn en passant par la Pologne et les pays baltes ! Pourquoi ce
voyage ? Eh bien pour la petite histoire, quand j'ai rencontré mathieu, nous
nous étions lancé le défi (complètement au pif) de faire Paris -
Saint-Pétersbourg à vélo. Ce que nous avons tenté en 2011, mais nous rendant
compte que 3 semaines pour faire 3500 kilomètres c'est un peu short, nous nous
étions arrêtés à Berlin et avions bifurqué sur le Danemark. BREF, pour fêter
les 10 ans de notre premier voyage, Berlin - Saint-Pétersbourg. Sauf que la
frontière russe est fermée, COVID tout ça, donc on s'arrête à Talinn. C
COMPLIKÉ mais voilà vous posez pas trop de questions.

Et donc départ de Paris ce matin très tôt, le plan étant de prendre un train
Paris - Francfort puis Francfort - Berlin et d'arriver vers 17h30. Ensuite,
une mini étape pour sortir de la grande ville et partir tranquillement le
lendemain. On avait même pris un uber van super cher pour pas galérer dans le
métro avec les vélos.  On arrive donc à la gare tranquille avec 20 minutes
d'avance. En ne voyant pas le numéro du quai, je rigole en disant que ce
serait marrant qu'avec toute cette organisation millimétrée pour la première
fois de notre carrière (en général on est plutôt complètement à la zeub), le
train soit en retard et qu'on rate notre correspondance.

:)

:)

:(

:'(

Les 10 minutes de retard se transforment en 40, puis en 50. La raison ? Pas
moins de 3 colis abandonnés sur les voies de départ du TGV, forçant à affrêter
un nouveau train sur une autre voie. Seule consolation, j'aime imaginer mamie
Huguette revenant des toilettes et trouvant sa petite valise à fleur
désintégrée par l'équipe de déminage.

En attendant le train est en retard et l'équipe allemande à bord qui aime
insister que c'est dans la gare FRANÇAISE que le retard s'est fait s'échine à
nous trouver des correspondances. Bizarrement, au lieu de nous proposer le
train suivant, ils nous demandent de faire une correspondance supplémentaire,
pas ouf avec les vélos très encombrants...

ET LA PLOT TWIST DE FOU, parce qu'en fait, on débarque à la gare de Francfort,
et on découvre que y'a une PUTAIN DE GRÈVE DES CHEMINOTS ALLEMANDS, les gars
font la grève une fois tous les 10 ans, et FORCÉMENT c'est le jour ou on part
qu'ils se disent "les mecs faut défendre nos droits obligé le 6 septembre
GEHEN SIE IHR ?" "Bien vu Shpuntz GRÖSSE IDEE" Bref, sur le retard de 50
minutes s'ajoute une correspondance de 50 minutes, au final, nous passons 6h
assis par terre entre 2 wagons ou entre 2 quais, le retour aux sources est
brutal. Mathieu qui doit finir un truc pour son travail s'attelle à la tache
entre les toilettes et la porte du wagon pendant que j'essaie de dormir en
devant me lever toutes les 5 minutes. Arrivés enfin à Berlin avec 2h30 de
retard, la nuit tombe, et les 24 kilomètres sympas prévus se transforment en
fast and furious pour essayer d'arriver le plus vite possible en pleine
nuit. 1h après, nous sommes dans un hôtel plutôt confortable parés à quitter
l'Allemagne dès demain !
