+++
title = "Jour 6"
date = 2021-09-11T10:17:43Z
draft = false
author = "Quentin"
cover = "day6.jpg"
place = "Gdansk"
summary = "La pluie ça mouille sa race"
map = true
+++

## Etape 

Bytow - Gdansk

## Kilomètres

100

## Météo

On a eu bien froid

## Phrase du jour

Tu peux me rendre mon masque que tu avais mis dans ton slip pour protéger ton
téléphone ? - Quentin

## Résumé

J'ai su que cette journée allait être compliquée en me levant. J'avais plutôt
bien dormi certes, mais l'hôtel était vendu avec un 'perfect breakfast' sur
Internet, et en arrivant dans la salle à manger, nous constatons qu'en fait il
est plutôt pas fou, 3 oeufs se battant en duel avec un peu de pain.

Le début de l'étape se passe parfaitement bien. Nous avançons et en profitons
pour faire des révisions simultanées, Mathieu de russe, moi de japonais. Et
soudainement sans prévenir, le voila, c'est bien l'orage ! Nous prenons une
première saucée violent avec des gouttes énormes qui fouettent bien. Essayant
de survivre dans une descente avec ses freins un peu limite quand mouillés,
Mathieu rate la bifurcation et nous devons remonter sur un bon kilomètre. Je
me rappelle à ce moment la que mon téléphone n'est pas étanche et qu'il est a
la merci des éléments déchainés sur le guidon de Mathieu pour faire le guidage
GPS. Las, il est trop tard et mon téléphone est en train de reboot en boucle
(ce qu'il fait encore actuellement 6h plus tard en attendant de se décharger
complètement). Nous devons utiliser le téléphone de Mathieu, qui n'est lui
aussi pas étanche. Pour lui éviter le même sort funeste que le mien, Mathieu
trouve une parade : il enveloppe son téléphone dans mon masque en tissu pour
le maintenir plus ou moins sec et le stocke entre son slip et son cuissard. Ça
fait bien longtemps qu'on se connait mais j'avoue que les frontières de
l'intimité ne sont pas loin d'être atteintes sur ce coup là...

Autre problème, nous utilisions mon téléphone car celui de Mathieu a choisi ce
voyage pour annoncer que sa batterie était complètement morte. Elle se vide en
quelques heures en veille, et encore plus rapidement quand utilisée. Nous
scotchons la batterie externe sur le cadre du vélo avec du sparadrap pour
essayer de gagner de quoi arriver à la fin de l'étape, ce qui fonctionne
plutôt bien malgré l'esthétique douteuse.

Profitant d'une éclaircie, nous nous arrêtons pour manger sous un arbre en
espérant que le temps reste sec une vingtaine de minutes. 2 minutes après nous
être assis, l'orage éclate a nouveau, et nous devons faire retraite sous un
porche pour manger en envisageant des prochains voyages en gros hummer tirant
une caravane de luxe plutôt qu'en vélo. Au moment de repartir, le ciel se
charge de nous envoyer un message avec des énormes grêlons de la taille de
glaçons. Impressionnés par ce message du ciel, j'improvise une épitre
religieuse remerciant le seigneur de nous accorder des épreuves pour tester
notre foi pendant que Mathieu ponctue de chants grégoriens et autres Ave
Maria.

C'est à ce moment la que le frein arrière de Mathieu lache complètement, et
qu'il réalise au même moment que le pédalier n'est plus du tout droit et doit
être resserré de toute urgence sous peine d'exploser. Nous resserrons son
frein avant en espérant qu'il suffise et pour le pédalier, on peut pas faire
grand chose donc on oublie. Dieu nous envoie une dernière épreuve sous forme
d'un pont au dessus d'une voie de train détruit et en travaux pour le
reconstruire de zéro avec des grosses grues moches. Nous descendons les vélos
sur les rails de train pour traverser en chantant les louanges du seigneur
pour qu'il ne nous envoie pas de TGV polonais sur la figure.

Nous finissons par arriver à Gdansk complètement trempés, sales comme jamais
avec de la terre partout (je vous ai pas dit, mais on s'est retapé du chemin
de terre, mais sous l'orage hihi). Pour éviter de complètement détruire
l'intérieur de notre location, je lave les sacoches dans une flaque du
caniveau, Mathieu optant pour la méthode ayant fait ses preuves du 'je me
douche avec mes sacoches pour nettoyer tout en meme temps'

Demain repos, enfin pas tout a fait car il faut qu'on trouve de quoi faire
réparer les vélos, mais au moins on roulera pas !
