+++
title = "Jour 20"
date = 2021-09-25T10:17:43Z
draft = false
author = "Mathieu"
cover = "day20.jpg"
place = "Talinn"
summary = "Final"
map = true
+++

## Etape 

Vändra - Talinn

## Kilomètres

107

## Météo

Froid, pluie, as usual.

## Phrase du jour

You need sleep - La dame de l'Airbnb qui a l'air de nous trouver fringants.

## Résumé

Je passe une nuit pas si mauvaise dans mon canapé dans la cabane dans
l'arbre. Dans un demi sommeil, j'entends le sol craquer à l'aube. Quentin
descend les marches. Il s'enfuit peut-être pour prendre un train en douce. Il
aurait bien raison, la pluie et le brouillard à l'extérieur ne donnent pas
envie de se lancer dans cette ultime étape.

Lorsqu'il revient une heure plus tard, je me dis qu'il a du changer d'avis. Il
s'avère qu'il a passé une bonne heure aux toilettes à l'autre bout du camp de
vacances pour mieux se remémorer quelques heures sombres d'Ouzbékistan.

Nous démarrons péniblement vers 10 heures direction Talinn. La première heure
est assez roulante sur une route déserte. Nous tombons sur un panneau
indiquant que la capitale est désormais à 75 kilomètres. Je sais que cela
signifie encore 4 heures d'effort alors que je suis déjà trempé.

L'itinéraire au moins est facile, il s'agit de suivre la route 15 sur toute la
durée restante. Comprenez, rouler à 20 kilomètres-heure sur une bande d'arrêt
d'urgence de 1 mètre. L'exercice est périlleux puisqu'un écart de quelques
centimètres à gauche ne sera pas pardonné par les semi-remorques Estoniens
qui nous dépassent sans beaucoup de considérations.

Quentin est bien brave car moins affaibli qui lui, je trouve déjà que cette
fin de voyage ne nous fait pas beaucoup de concessions. La stratégie
maintenant éprouvée du
je-roule-sans-m'arrêter-pendant-5-heures-parce-que-j'ai-froid fonctionne et
nous arrivons à Talinn vers 15 heures. 

Cela nous laisse le temps de mettre au point un stratagème plus subtil pour
les prochains voyages. Il consiste à louer une trottinette électrique puis à
tracter son compère à vélo via une sangle. Les résultats sont peu concluants
mais nous avons bon espoir de parvenir à améliorer la technique.

Et voilà pour ce voyage que nous pensions facile et qui s'est avéré pas si
facile. En essayant de prendre un peu de recul, peut-être que nous avons un
peu moins envie de nous faire mal avec les années. Les 25 heures de bus et 12
heures de train du retour me permettront sûrement de mûrir ma réflexion.

Merci de nous avoir lus :)

---

Quentin: plus jamais jusqu'à la prochaine fois, merci d'avoir suivi nos
aventures encore une fois !
