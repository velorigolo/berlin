+++
title = "Jour 10"
date = 2021-09-15T10:17:43Z
draft = false
author = "Quentin"
cover = "day10.jpg"
place = "Wegorzewo"
summary = "La piste cyclable"
map = true
+++

## Etape 

Pieniezno - Wegorzewo

## Kilomètres

125

## Météo

Il commence a faire bieeeen frais

## Phrase du jour

Regarde sur la piste cyclable je vais plus vite que toi sur la route ! -
Mathieu qui force comme un âne pour avoir raison.

## Résumé

### INTRODUCTION

Avant de vous raconter cette étape, il faut vous raconter l'histoire du vélo
de Mathieu. En arrivant a Gdansk il y'a quelques jours, nous nous sommes
rendus compte que son vélo (qui est en fait mon ancien vélo de randonnée, qui
a presque 10 ans et un sacré nombre de km), a son axe de pédalier complètement
explosé, roulement a bille détruit et ainsi de suite. Nous l'apportons à un
réparateur de vélo très sympa qui accepte de changer la pièce manquante malgré
les 3 semaines de liste d'attente. En vérifiant le vélo, il réalise que le
dérailleur est monté à l'envers (même si il marche bien), et décide de le
remettre à l'endroit. Problème, en faisant ça, il oublie dans le rush de le
régler et hier, Mathieu a réalisé qu'il lui était impossible de passer aux
premiers et troisième plateau... Hier soir après l'étape, il décide de tenter
une réparation. Au bout d'une heure à galérer, il semble satisfait et nous
allons manger. Mais en démarrant ce matin, son dérailleur frotte à fond et
c'est clairement pas possible de faire une grosse étape comme ça. Mathieu
passe donc 45 minutes ce matin avant de réaliser qu'en fait, il galère pour
absolument rien et qu'il suffisait de déserrer le cable. Au moins nous avons
appris et le problème est réglé. Cependant nous partons à 10h30 bien tapées,
et nous avons plus de 120 kilomètres à faire. Il va donc falloir être
efficace.

### FIN DE L'INTRODUCTION

Au menu du jour : pas de chemin mais une route qui semble très bien sur google
maps mais n'est pas sur notre application de guidage. On ne comprends pas trop
mais on décide que YOLO et on y va. De toute façon, on a pas vraiment
d'alternative sans faire un énorme détour. Arrivés sur la route, nous
saisissons vite le soucis : la route est en réfaction TOTALE. A des endroits,
elle est complètement neuve, à d'autres, le goudron de l'ancienne route à été
tout simplement retiré et nous retrouvons notre ami la terre qui fait trembler
les lunettes de soleil et tomber mes sacoches. Au total, c'est pas moins de 50
kilomètres de route qui est en travaux. Les ouvriers nous regardent avec
amusement pendant que nous tentons de dépasser camions de transports et autres
bulldozers. Cependant nous avançons bien et arrivons dans une plus grande
ville pour manger. La joie est de courte durée, car nous réalisons qu'il nous
reste un bon 75 kilomètres à parcourir. Mathieu tient tout de même à faire une
sieste réparatrice puis à envoyer des messages en rigolant à ses amis sur
Whattsap. Pour tuer le temps, je fais 3 allers retours à la poubelle du
supermarché pour jeter nos déchets.

Et c'est reparti pour du bon gros sprint sans aucune pause. Le reste de
l'étape est très smooth à part 2 moments. Le premier, c'est quand nous
arrivons à un nouveau pont en réparation et donc inutilisable, pas mal c'est
le deuxième en une semaine de vélo. Heureusement, les polonais plein de
ressources ont installé une petite barque reliée à des cordes pour faire
office de bac artisanal. Nous hissons les vélos dans la barque d'une manière
très artistique. Mathieu est impressionné par mon pied marin légendaire quand
je m'amuse à faire un double salto suivi d'une pirouette acrobatique en
retombant sur la proue du navire. Le deuxième moment, c'est quand les insectes
nous assaillent. J'en ai déjà parlé, mais c'est un cauchemar. Mon bras est
NOIR de cadavres de moucherons (regardez les photos du jour). La grande
majorité meurent en percutant mes gros bras poilus pleins de sueur et de crème
solaire à 20 km/h, mais ce qui est terrible ce sont ceux qui survivent et se
débattent, ce qui me gratte de ouf. Pas très agréable, surtout quand tu passes
2 minutes à enlever la nuée de moucherons précédente de ton corps pour t'en
retaper une 30 secondes après.

Vers la fin de l'étape, nous rencontrons une jolie piste cyclable, mais qui a
comme défaut de bien plus épouser les formes du terrain que la
route. Comprenez que ça monte et ça descend beaucoup plus. Après quelques
kilomètres, j'en ai marre et décide de retourner sur la route. Mathieu me
tient alors un discours comme quoi la piste cyclable, non seulement c'est plus
sécuritaire, mais on va également plus vite dessus. Après quoi il se met à
sprinter comme un âne pour aller plus vite que moi. Je n'avais cependant pas
trop compris ce qu'il essayait de faire, pensant juste qu'il voulait arriver
vite à la fin de l'étape. Je continuais ainsi à lui faire des blagues,
notamment sur un nom de ville qui ressemblait à Balkany. C'est devant son
manque de réaction que je me décide à l'observer complètement rougi par
l'effort de grimper les côtes de la piste cyclable au troisième plateau pour
aller plus vite que moi. Bon perdant, j'admets ma défaite suite à quoi Mathieu
passera la fin de l'étape à m'expliquer que j'ai eu tort de ne pas faire
confiance à la piste cyclable qui roule vite et que je devrais m'inscrire à sa
nouvelle association LPPCQMQD (Ligue de protection des pistes cyclables qui
montent et qui descendent) et à s'accrocher à mon vélo en se plaignant que
c'est trop long et que le vélo c'est nul. Après une grosse étape, nous
arrivons, OUF !

