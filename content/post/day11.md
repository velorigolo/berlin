+++
title = "Jour 11"
date = 2021-09-16T10:17:43Z
draft = false
author = "Mathieu"
cover = "day11.jpg"
place = "Wiżajny"
summary = "Rêveries circulaires"
map = true
+++

## Etape 

Wegorzewo - Wiżajny

## Kilomètres

95

## Météo

Temps d'escargot

## Phrase du jour

J'ai froid - Quentin

## Résumé

Petit voyageur esseulé dans un monde accéléré  
Lancé sur un asphalte qui ne t'es pas destiné  
Témoin privilégié des campagnes déshéritées  
De toutes les divinités, à Éole tu es inféodé  

Côtoyant les bolides endiablés,  
ton frêle esquif n'a que peu d'autorité  
Esclave de la machine aux essieux chromés  
Toute ton énergie tu dois lancer  
pour mouvoir tes rouages abîmés   

Conquérant de l'inutile ou futile passager  
d'un voyage ou rien n'est à gagner.  

Mathieu

---

Début d'étape très bucolique sur une petite route départementale déserte, je
suis d'humeur rêveuse et mets à profit mon temps pour composer un petit
poème. L'hiver n'est plus très loin et j'ai enfilé toutes mes épaisseurs en
partant ce matin.

Quentin persiste dans le style t-shirt - cuissard court, car comme il aime à
le rappeler, il a fait l'Islande en short. Il enfile néanmoins une veste puis
achète une doudoune dans un Lidl avant de me parler du poids des années.

Nous roulons bien et sentant qu'une grosse averse nous pend au nez, nous
dérogeons à la sacro-sainte pause de midi. Il ne nous reste qu'une bonne
quarantaine de kilomètres avant notre petite auberge. Nous y arrivons à 15
heures, trempés et affamés.

La logeuse ne parle pas un mot d'anglais, c'est l'occasion de tester un peu
mon Russe. Nous quittons la Pologne demain pour la suite du périple en
Lituanie !

