+++
title = "Jour 9"
date = 2021-09-14T10:17:43Z
draft = false
author = "Mathieu"
cover = "day9.jpg"
place = "Pieniężno"
summary = "La bouillasse"
map = true
+++

## Etape 

Gdansk - Pieniężno

## Kilomètres

120

## Météo

Bon le matin, pluvieux l'après midi

## Phrase du jour

Le karma frappe toujours après manger - Mathieu

## Résumé

Petite étape à Gdansk qui nous permet de récupérer un peu et de réparer mon
vélo. Le réparateur de vélo me confie alors que mon pédalier est "completely
exploded" pour citer ses termes. Il a rarement vu ce type de casse, c'est
pourtant la seconde fois que cela m'arrive. Cette réflexion me conduit à
questionner ma vie et mon oeuvre.

Le problème est que l'atelier de réparation a une liste d'attente de plusieurs
semaines. Devant mon insistance, le réparateur accepte d'envisager de le
réparer dans la journée. Nous nous retrouvons au final à réparer mon vélo, qui
est en fait l'ancien vélo de Quentin, à 8 heures du soir dans son atelier.

Le vélo fin prêt, nous pouvons reprendre la route. Pour éviter que la
malchance frappe à nouveau, Quentin brûle quelques cierges dans la magnifique
Église Sainte Marie de Gdansk. D'un naturel prudent, il entend cependant
maximiser nos chances et me demande ce que je pense d'une séance de
magnétisme. N'étant pas bien sur de vouloir poursuivre le voyage a tout prix,
je refuse.

Nous voilà donc à nouveau sur les routes ce matin, dans la fraîcheur de cette
fin d'été Polonais. Le début d'étape est extrêmement roulant et nous avons
avalé la moitié de l'étape à la pause de midi. Je fais alors remarquer à
Quentin, que la malchance frappe essentiellement en seconde partie de journée.

Quelques minutes après être repartis, la route se transforme en chemin sans
que nous aillons le temps de crier gare. Le chemin est tellement boueux que je
m'enfonce presque jusqu'aux genoux. Des arbres récemment abattus viennent
compléter l'épreuve. Nous mettons quasiment une heure à faire 3 kilomètres.

De retour sur la route, je constate que mes dérailleurs ne fonctionnent
plus. Je suis bloqué sur le second plateau et la seule façon de passer une
vitesse à l'arrière est d'appuyer 3 ou 4 fois sur la manette de changement de
vitesse puis de passer dans un gros trou.

J'espère que le vélo acceptera de continuer jusqu'à la prochaine pause pour de
plus amples réparations!
