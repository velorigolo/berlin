+++
title = "Jour 2"
date = 2021-09-07T10:17:43Z
draft = false
author = "Mathieu"
cover = "day2.jpg"
place = "Szczecin"
summary = "Polska !"
map = true
+++

## Etape 

Bernau - Szczecin

## Kilomètres

122

## Météo

Bonne

## Phrase du jour

Kurva! - Quentin qui se fait complimenter en Polonais.

## Résumé

Départ tardif pour cette première vraie étape. Histoire de se mettre dans le
bain gentiment nous avons opté pour une courte étape de 120 kilomètres,
direction la Pologne. Non en vrai, c'est beaucoup et nous avons un peu la
pression. Le début d'étape s'effectue dans le silence. Heureusement après la
pause de midi, nous retrouvons quelques réflexes. C'est peut être à cause d'un
précédent voyage de 7000 kilomètres.

Dans l'après midi, nous quittons l'Allemagne par des pistes de foret bien
accidentées pour rentrer en Pologne. Nous avons tellement bien préparé ce
voyage que Quentin a oublié tout ses papiers au Mans et mise très fort sur sa
carte d'adhésion au club de gym pour prouver son identité. J'ai pour ma part
une carte d'identité périmée depuis deux ans.

La frontière est bien tranquille et seule une voiture de police semble
stationnée aux environs. Comme il serait trop dommage de quitter l'Allemagne
sans se faire remarquer nous escaladons le panneau frontalier avant de
déguerpir.

Nous nous rendons compte que nous connaissons très peu de choses sur la
Pologne. Les tarifs affichés de l'essence nous font un instant craindre qu'il
ne s'agisse d'un pays affreusement cher peuplé de vaches et de mangeurs de
chocolat à l'instar de la Suisse. Nous découvrons avec soulagement qu'ils
n'utilisent juste pas l'euro et que cela ressemble plus aux autres pays de
l'ancien bloc soviétique que à la confédération helvétique.

Quentin profite des longues avenues monotones pour m'expliquer qu'il s'est
beaucoup assagi et qu'il témoigne désormais d'un respect maximal envers son
prochain et le genre humain en général. Quelques bloc plus loin, il manque de
se faire écraser par une voiture et deux semi-remorques avant de se faire
copieusement insulter en Polonais. Il a quand même l'air de bonne foi, je lui
laisse le bénéfice du doute.
