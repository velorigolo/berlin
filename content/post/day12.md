+++
title = "Jour 12"
date = 2021-09-17T10:17:43Z
draft = false
author = "Quentin"
cover = "day12.jpg"
place = "Kaunas"
summary = "Record"
map = true
+++

## Etape 

Wizajni - Kaunas

## Kilomètres

115

## Météo

Il pleut il mouille c'est pas la fête aux cyclistes

## Phrase du jour

'Kaunas !' 'Euh m'insulte pas s'il te plait'

## Résumé

Le soucis avec le vélo de randonnée, c'est que le nombre de critères pour
qu'une étape se passe bien sont nombreux :

- Ne pas avoir de soucis techniques (très courant)
- Une bonne route bien asphaltée (plutôt courant)
- Ne pas se perdre pendant l'étape (plutôt courant)
- Avoir une bonne météo (moyennement courant)
- Ne pas avoir de vent de face (moyennement courant)

Même si les ennuis sont finalement plutôt rares, il suffit d'un seul critère
pour rendre l'étape pénible. Dans le cas de ce voyage, nous avons eu par
exemple la majorité des étapes gâchées par des chemins dégueulasses. Depuis 3
jours, plus de chemins, mais Mathieu a eu des soucis techniques il y'a 3
jours, et hier et aujourd'hui, la météo était horrible. Pas une seule journée
sur les 9 étapes déjà faites avons-nous été tranquilles, ce qui est quand même
un peu relou avouons-le.

Aujourd'hui donc, nous sortons de notre petit refuge pour la nuit et sommes
accueillis par une fine bruine de brouillard et 10 degrés au
compteur. Concrètement il ne pleut pratiquement pas, mais avec la vitesse du
vélo et les flaques, c'est tout comme. Pour nous donner du courage, notre hôte
polonais nous donne de la liqueur maison au petit déjeuner - avec un pain
maison délicieux, deux fois. C'est dur mais après le lait à l'œuf de
l'Ouzbékistan qui m'avait donné une intoxication alimentaire, je tiens plutôt
bien le coup.

Nous arrivons assez vite à la frontière (déjà mouillés) avec la Lituanie et
disons au revoir à la Pologne qui nous aura accueillis pendant plus de la
moitié du voyage. Je remarque assez vite que Mathieu semble mal tenir la
double dose d'alcool artisanal. Pour éviter qu'un camion lituanien lui fasse
un accueil percutant, je décide de faire bouclier entre lui et la route. Ma
technique : rouler au milieu de la route, ce qui force les voitures à dépasser
en se décalant complètement sur la voie opposée. Quelques conducteurs
récalcitrants klaxonnent leur énervement à perdre 5 secondes de leur vie pour
dépasser en toute sécurité, mais j'avoue qu'avec 10 ans de vélo quotidien dans
Paris, je m'en contrebasse légèrement les noix (je reste poli car nos parents
lisent le blog). Au moins Mathieu est bien protégé et me remercie en
m'expliquant pourquoi il est déçu de moi (une des raisons étant : je suis déçu
que tu n'aimes pas le quinoa).

Parti en t-shirt + imperméable, j'ai assez vite froid et dois accepter que mon
âge vénérable de 32 ans ne me permet plus de rester à poil quand il fait 10
degrés, et j'enfile la polaire. Marrant comme tout va mieux quand on a
chaud. Nous avançons bien malgré le vent de face, et ne pensons même pas à
nous arrêter pour manger à midi (d'abord car il fait très froid, ensuite parce
que nous sommes toujours sous l'effet du petit déjeuner et liqueur du
matin). Nous mangeons donc à la place les 115 kilomètres sans aucune pause, ce
qui est notre nouveau record (mais on aimerait bien qu'il fasse un peu
meilleur pour pouvoir nous poser à midi la prochaine fois)

Demain jour de repos à Kaunas, je vais m'acheter un nouveau téléphone, parce
que l'idée d'un téléphone de Kaunas me fait beaucoup rire (et aussi parce que
le téléphone de Mathieu a également décédé et que notre vieux téléphone ne
fait plus les points GPS, oui on s'amuse bien youpi). A bientôt !
