+++
title = "Jour 18"
date = 2021-09-23T10:17:43Z
draft = false
author = "Mathieu"
cover = "day18.jpg"
place = "Ainazi"
summary = "La reprise"
map = true
+++

## Etape 

Riga - Ainazi

## Kilomètres

100

## Météo

Bien froid mais ensoleillé.

## Phrase du jour

Zut j'ai oublié la phrase du jour - Mathieu

## Résumé

Depuis plus de 10 ans que nous voyageons ensemble, nous commençons a avoir
vécu un certain nombre d'expériences.  Pour autant, après toutes ces années,
le terme "préparation méticuleuse" est sûrement celui qui décrit le plus mal
nos voyages. En général, le processus de décision est assez élaboré. Nous nous
retrouvons dans un restaurant, une idée me passe par la tête, je lance : ça te
dirait d'aller en Russie en vélo ? Quentin recrache son sushi pour exprimer
son enthousiasme.

Dans les quelques mois qui précèdent le voyage, l'un des deux finit toujours
par appeler l'autre pour lui dire : je regarde un reportage Arte sur la Russie
on dirait qu'il fait froid et que c'est la guerre à la frontière. L'autre
répond : ah ouai, heureusement que t'as vu ça, on va emmener une polaire et un
couteau suisse.

N'ayant pas la résistance physique d'un Mike Horn ou la force de caractère
d'un Sylvain Tesson, nous finissions quasi systématiquement par tomber malade,
nous blesser et changer complètement de plan. A vrai dire, à part pour
l'ascension du mont Blanc en 2012, nous n'avons jamais réalisé parfaitement
l'objectif fixé initialement.

Sur ce voyage, c'est d'abord la casse de l'axe de mon pédalier qui a failli
nous faire renoncer. Ayant pu réparer de justesse, je pensais alors que nous
étions bien partis pour vaincre la malédiction. Hélas, c'était sa compter sur
la traditionnelle crève dont a été victime Quentin. Trois bons jours de
fièvre, de la toux qui nous forcent à relier Kaunas à Riga en bus.

Nous voilà quand même repartis ce matin, par quatre degrés et dans un vent
glacial. Quentin est toujours en short, mais il a acheté des moufles en laine
avec des chats cousus dessus pour rester bien au chaud. Le début d'étape est
compliqué, cependant nous sommes pour un fois un peu aidés par le triptyque
jusqu'alors inédit : bonne route - vent dans le dos - pas de dénivelé.

Vers la fin de l'étape, je fais remarquer à Quentin que la route est vraiment
très bonne. 300 mètres plus loin de gros panneaux signalent des travaux et
elle se transforme en chemin. Je devrais parfois vraiment me taire.

Nous arrivons vers 14 heures dans la maison que nous avons loué faute d'hôtels
aux alentours. Je vais mettre a profit ce repos pour choisir notre prochain
voyage, cette fois inratable. J'oscille pour l'instant entre le tour de
l'étang de Boulogne en barque ou la traversée de la promenade des Anglais en
roller. N'hésitez pas à proposer vos idées dans les commentaires !

