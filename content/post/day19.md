+++
title = "Jour 19"
date = 2021-09-24T10:17:43Z
draft = false
author = "Quentin"
cover = "day19.jpg"
place = "Vändra"
summary = "Le mental"
map = true
+++

## Etape 

Ainazi - Vändra

## Kilomètres

108 kilomètres

## Météo

Bien froid et humide comme en Islande

## Phrase du jour

Bon soit on prend une quasi autoroute à camions soit on prend un chemin boueux
qui rallonge tu préfères quoi ? - Quentin qui présente les options du jour

## Résumé

Dur dur aujourd'hui pour votre serviteur. La maladie m'a laissé bien affaibli
physiquement, et les 2 journées de récupérations à Riga n'étaient
semblent-t'elles pas suffisantes. Hier c'était limite, et j'y ai passé toutes
les réserves que j'avais reconstitué. Du coup aujourd'hui c'était très, très
dur. Au bout du 30iè kilomètre je sens que les jambes n'y sont pas et je
compte chaque kilomètre. La présence de bornes très précises qui m'expliquent
parfaitement bien que j'avance plus lentement que d'habitude ne font
qu'ajouter à la détresse.

À un moment, je demande à Mathieu de faire une pause (ce qui n'arrive que
rarement). Il reste plus de la moitié de l'étape à
parcourir. Dur. Heureusement pour moi, la route est bonne et pour la première
fois de ce voyage, aucun chemin boueux ou moche ne se présente à nous. Mathieu
est bon joueur et accepte de rouler à une allure plus réduite. Le voir faire
du vélo à une main en mettant un coup de pédale tous les 10 mètres pendant que
je souffle comme un âne courbé sur mon guidon m'aide à me motiver.

Le pire dans cette étape n'est pas tant la fatigue que mon état. Mon corps qui
aimerait rester un peu tranquille, merde quoi, se mets à suer au moindre
effort. Au bout de quelques kilomètres, mon t-shirt est trempé et avec la
température de 8 petits degrés, je me pèle sacrément le fion. Tout au long de
l'étape, je change 3 fois de t-shirts. Las, ce n'est pas très efficace et
globalement j'ai extrêmement froid. Alors que d'habitude je suis en t-shirt et
mathieu en polaire, c'est pour une fois moi qui suis plus couvert que ce
dernier, qui tente même quelques kilomètres sans veste (avant de rapidement la
remettre faut pas pousser).

Vient le moment de finir l'étape par une route nationale, pas super cool
certes, mais seule alternative a 20km de chemins (avec la pluie de la veille :
infernal). Mathieu qui déteste se faire raser par les camions (on le comprend)
aimerait bien tenter les chemins mais me prend en pitié en voyant mon piteux
état. Je trompe la distance restante en imaginant que je fais des allers
retours à vélo dans Paris.

Arrivés dans la petite ville de Vändra, je me rend compte que je n'ai pas loué
une chambre d'hôtel mais un complexe complet dans un spa. Nous avons 3
bâtiments pour nous dont une cabane dans les arbres pour dormir et un
sauna. Super. Demain dernière étape, il faut tenir le coup !
